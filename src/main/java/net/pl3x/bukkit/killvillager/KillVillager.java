package net.pl3x.bukkit.killvillager;

import net.pl3x.bukkit.killvillager.command.CmdKillVillager;
import net.pl3x.bukkit.killvillager.configuration.Config;
import net.pl3x.bukkit.killvillager.configuration.Lang;
import net.pl3x.bukkit.killvillager.listener.VillagerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class KillVillager extends JavaPlugin {
    private boolean hasGriefPrevention = false;

    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (Bukkit.getPluginManager().isPluginEnabled("GriefPrevention")) {
            hasGriefPrevention = true;
            Logger.info("Hooking into GriefPrevention.");
        }

        Bukkit.getPluginManager().registerEvents(new VillagerListener(this), this);

        getCommand("killvillager").setExecutor(new CmdKillVillager(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public boolean hasGriefPrevention() {
        return hasGriefPrevention;
    }

    public static KillVillager getPlugin() {
        return KillVillager.getPlugin(KillVillager.class);
    }
}

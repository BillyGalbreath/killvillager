package net.pl3x.bukkit.killvillager.listener;

import net.minecraft.server.v1_11_R1.InventoryMerchant;
import net.minecraft.server.v1_11_R1.Item;
import net.pl3x.bukkit.killvillager.KillVillager;
import net.pl3x.bukkit.killvillager.Logger;
import net.pl3x.bukkit.killvillager.configuration.Config;
import net.pl3x.bukkit.killvillager.hook.GriefPreventionHook;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftInventoryMerchant;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionType;

import java.util.Random;


public class VillagerListener implements Listener {
    private final KillVillager plugin;
    private final Random random = new Random();

    public VillagerListener(KillVillager plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onVillagerProtect(EntityDamageEvent event) {
        if (!Config.ONLY_KILL_WITH_POISON) {
            return;
        }

        if (event.getEntity() instanceof Villager) {
            if (((Villager) event.getEntity()).getProfession() == Villager.Profession.NITWIT) {
                return; // lets damage nitwits
            }
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTradeClosed(InventoryCloseEvent event) {
        InventoryHolder holder = event.getInventory().getHolder();
        if (!(holder instanceof Villager)) {
            return; // not a villager trade menu
        }

        if (!(event.getPlayer() instanceof Player)) {
            return; // not a player (possible? o_O)
        }

        Player player = (Player) event.getPlayer();
        Villager villager = (Villager) holder;
        Logger.debug(event.getPlayer().getName() + " closed a villager trade menu at " + villager.getLocation());

        // check for griefprevention
        if (plugin.hasGriefPrevention() && !GriefPreventionHook.isAllowedToKill(villager, player)) {
            Logger.debug("GriefPrevention protected this villager.");
            return; // grief prevention protected
        }

        // check for and remove poison item
        if (!hasPoison(event.getInventory())) {
            Logger.debug("Villager was not given poison.");
            return;
        }

        // kill villager
        Logger.debug("Villager killed with poison.");
        ItemStack itemDrop = Config.VILLAGER_DROPS.clone();
        itemDrop.setAmount(random.nextInt(itemDrop.getAmount() + 1)); // chance to get 0 - max
        if (itemDrop.getAmount() > 0) {
            // dont drop empty stacks
            villager.getWorld().dropItem(villager.getLocation(), itemDrop);
        }
        if (random.nextInt(2) == 1) {
            villager.getWorld().dropItem(villager.getLocation(), new ItemStack(Material.GLASS_BOTTLE));
        }
        villager.remove();
    }

    private boolean hasPoison(Inventory inventory) {
        boolean poisoned = false;

        try {
            InventoryMerchant craftInv = ((CraftInventoryMerchant) inventory).getInventory();
            ItemStack recipe1 = CraftItemStack.asBukkitCopy(craftInv.getItem(0));
            ItemStack recipe2 = CraftItemStack.asBukkitCopy(craftInv.getItem(1));
            if (isPoison(recipe1)) {
                craftInv.setItem(0, new net.minecraft.server.v1_11_R1.ItemStack((Item) null));
                poisoned = true;
                Logger.debug("Slot 1 contains poison.");
            }
            if (isPoison(recipe2)) {
                craftInv.setItem(1, new net.minecraft.server.v1_11_R1.ItemStack((Item) null));
                poisoned = true;
                Logger.debug("Slot 2 contains poison.");
            }
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
        return poisoned;
    }

    private boolean isPoison(ItemStack itemStack) {
        // check itemstack
        if (itemStack == null || !itemStack.hasItemMeta()) {
            return false; // no metadata
        }

        // check if potion
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (!(itemMeta instanceof PotionMeta)) {
            return false; // not a potion
        }

        // check if poison type
        return ((PotionMeta) itemMeta).getBasePotionData().getType() == PotionType.POISON;
    }
}

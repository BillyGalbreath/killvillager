package net.pl3x.bukkit.killvillager.configuration;

import net.pl3x.bukkit.killvillager.KillVillager;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static boolean ONLY_KILL_WITH_POISON = true;
    public static ItemStack VILLAGER_DROPS = new ItemStack(Material.EMERALD, 3);

    public static void reload() {
        KillVillager plugin = KillVillager.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        ONLY_KILL_WITH_POISON = config.getBoolean("only-kill-with-poison", true);

        String dropType = config.getString("villager-drops.type", "EMERALD");
        byte dropData = (byte) config.getInt("villager-drops.data", 0);
        int quantity = config.getInt("villager-drops.quantity", 3);
        Material dropMaterial = Material.matchMaterial(dropType);
        //noinspection deprecation
        VILLAGER_DROPS = new ItemStack(dropMaterial, quantity, (short) 0, dropData);
    }
}

package net.pl3x.bukkit.killvillager.hook;

import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.DataStore;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

public class GriefPreventionHook {
    private static DataStore dataStore;

    private static DataStore getDataStore() {
        if (dataStore == null) {
            dataStore = GriefPrevention.getPlugin(GriefPrevention.class).dataStore;
        }
        return dataStore;
    }

    public static boolean isAllowedToKill(Villager villager, Player player) {
        Claim claim = getDataStore().getClaimAt(villager.getLocation(), false, null);
        return claim != null && claim.allowBuild(player, null) == null;
    }
}
